from abc import ABC, abstractmethod


class Switchable(ABC):
    """This is the abstract class from the other classes will inherit methods"""
    @abstractmethod
    def turn_on(self):
        pass

    @abstractmethod
    def turn_off(self):
        pass


class LightBulb(Switchable):
    """This is a class that inherits methods from switchable and implements this methods"""
    def turn_on(self):
        print("LightBulb: turned on....")

    def turn_off(self):
        print("LightBulb: turned off....")


class DumbTv(Switchable):
    """This is a class that inherits methods from switchable and implements this methods"""
    def turn_on(self):
        print("DumbTv: turned on....")

    def turn_off(self):
        print("DumbTv: turned off....")


class ElectricPowerSwitch:
    """This is a class that take a switchable object and create an interface for a better use"""
    def __init__(self, c: Switchable):
        self.lightBulb = c
        self.on = False

    def press(self):
        if self.on:
            self.lightBulb.turn_off()
            self.on = False
        else:
            self.lightBulb.turn_on()
            self.on = True


# declarations
lio = LightBulb()
dio = DumbTv()
# implement the power switch
switch = ElectricPowerSwitch(lio)
switchtv = ElectricPowerSwitch(dio)
# use of the interfaces
switch.press()
switch.press()
switchtv.press()
switchtv.press()
